require(["dojo/_base/lang",
         "icm/action/Action","icm/base/Constants", "icm/model/properties/controller/ControllerManager","dojo/dom-attr","dojo/dom-style"], function(lang, Action,Constants, ControllerManager,domAttr,domStyle){
    lang.setObject("ccresponseaction", {
        "passthrough": function(payload, solution, role, scriptAdaptor){
            return payload;
        },
		
           
        decisionaction: function(payload, solution, role, scriptAdaptor){
        	try {
        		
        		
        		
        	    var responseself = scriptAdaptor;
        	    console.log("***** responseself *****");
        	    console.log(responseself);
        	    var coordination = payload.coordination;
        	        responseself.editable = payload.workItemEditable;
        	        var solution = responseself.solution;
        	        var prefix = solution.getPrefix();
        	        var userName = ecm.model.desktop.userDisplayName;
        	        var userId = ecm.model.desktop.userId;
        	        var role = ecm.model.desktop.currentRole.name;
        	        
        	        removeResponseButtons(role,responseself.editable);
        	        
        	        require(["icm/base/Constants","icm/model/properties/controller/ControllerManager"], function(Constants, ControllerManager){
        	        	
        	        	//METHOD WHICH IS CALLED BEFORE LOADING WIDGET IS FINISHED
        	        	coordination.participate(Constants.CoordTopic.BEFORELOADWIDGET, function(context, complete, abort) {
        	        		/* Obtain the controller binding for the editable. */
        	        		if(!responseself.hasOwnProperty('propsController')){
        	        		responseself.propsController = ControllerManager.bind(responseself.editable);
        	        		 var step=responseself.editable.icmWorkItem.stepName;
        	        		
        	            	var theController=responseself.propsController;
        	            
        	        		
        	        		if(role === "Assigner"){
        	            		beforeloadaction.beforeloadactionAO(theController,payload, this.solution, this.role, scriptAdaptor);
        	            	}
        	            	else if(role === "Loan Processing Officer"){
        	            		beforeloadaction.beforeloadactionLPO(theController,payload, this.solution, this.role, scriptAdaptor);
        	        			
        	        		}else if(role === "GM SPARE PARTS"){
        	        			beforeloadaction.beforeloadactionGSP(theController,payload, this.solution, this.role, scriptAdaptor);
        	        			
        	        		}else if(role === "GM FINANCE"){
        	        			beforeloadaction.beforeloadactionGMF(theController,payload, this.solution, this.role, scriptAdaptor);
        	        			
        	        		}else if(role === "WH CLERK"){
        	        			beforeloadaction.beforeloadactionWHC(theController,payload, this.solution, this.role, scriptAdaptor);
        	        		}else if(role === "WH MANAGER"){
        	        			beforeloadaction.beforeloadactionWHM(theController,payload, this.solution, this.role, scriptAdaptor);
        	        		}else if(role === "GRN CLERK"){
        	        			beforeloadaction.beforeloadactionGRC(theController,payload, this.solution, this.role, scriptAdaptor);
        	        			
        	        		}else if(role === "INVENTORY CLERK"){
        	        			beforeloadaction.beforeloadactionIC(theController,payload, this.solution, this.role, scriptAdaptor);
        	        		}else if(role === "AP Clerk"){
        	        			beforeloadaction.beforeloadactionAPC(theController,payload, this.solution, this.role, scriptAdaptor);
        	        			
        	        		}else if(role === "Finance HOD"){
        	        			beforeloadaction.beforeloadactionFHO(theController,payload, this.solution, this.role, scriptAdaptor);
        	        			
        	        		}else if(role === "Internal Audit"){
        	        			beforeloadaction.beforeloadactionIAB(theController,payload, this.solution, this.role, scriptAdaptor);
        	        			
        	        		}else if(role === "Internal Audit BU"){
        	        			beforeloadaction.beforeloadactionIAB(theController,payload, this.solution, this.role, scriptAdaptor);
        	        			
        	        		}else if(role === "Finance Clerk HO"){
        	        			beforeloadaction.beforeloadactionFCH(theController,payload, this.solution, this.role, scriptAdaptor);
        	        			
        	        		}else if(role === "Internal Audit BU"){
        	        			beforeloadaction.beforeloadactionIAH(theController,payload, this.solution, this.role, scriptAdaptor);
        	        			
        	        		}else if(role === "Internal Audit HO"){
        	        			beforeloadaction.beforeloadactionIAH(theController,payload, this.solution, this.role, scriptAdaptor);
            	        		
        	        			
        	        		}else if(role === "Finance Clerk"){
        	        			beforeloadaction.beforeloadactionFCH(theController,payload, this.solution, this.role, scriptAdaptor);
        	        			
        	        		}
        	        		
        	        		}
        	        		/* Call the coordination completion method. */
        	        		complete();
        	        		});
        	        	
        	        	
        	        	//METHOD WHICH IS CALLED WHEN LOADING WIDGET IS FINISHED
        	        	coordination.participate(Constants.CoordTopic.AFTERLOADWIDGET, function(context, complete, abort) {
        	        		/* Obtain the controller binding for the editable. */
        	        		
        	        		
        	        		 /*if(payload.caseEditable != undefined){
        	        				caseEdit=payload.caseEditable;
        	        				page="addCase";
        	        			}else if(payload.workItemEditable){
        	        				caseEdit=payload.workItemEditable;
        	        				page="workDetail";
        	        			}*/
        	        		
        	        		/*
        	        			
        	        		var coord = payload.coordination;
        	        		var solution = this.solution;
        	        		var prefix = "IPS";
//alert("before req");
        	        		

        	        		if(coord){
//alert("1");
        	        		//coord.participate(Constants.CoordTopic.LOADWIDGET, function(context, complete, abort) {
  //      	alert("2");        		        
        	        		        if (payload.workItemEditable.icmWorkItem.ecmWorkItem.attributes.hasOwnProperty("IPS_Status")){
    //    	        	alert("3");
        	        		        	var automated = payload.workItemEditable.icmWorkItem.ecmWorkItem.attributes["IPS_Status"];
        	        		                alert(automated);                
        	        		                if(automated){
        	        		                                          
        	        		                 var nodeSearch = dojo.query('.icmCaseSearch icmPageWidget');

        	        		                                 
        	        		                        //         domStyle.set(nodeSearch[0].domNode, 'display', 'none');
        	        		                                }
        	        		                
        	        		                                        
        	        		                                                         }
        	        		                                                         
        	        		                                                         

        	        		  complete();
        	        		// });
        	        		}*/
        	        		
        	        		
        	        		
        	        		
        	        		
        	        		
        	        		if(!responseself.hasOwnProperty('propsController')){
        	        		responseself.propsController = ControllerManager.bind(responseself.editable);
        	        		
        	        		}
        	        	
        	        		
        	        		if(role === "PO Clerk"){

        	        			dashboardscript.getAdvanceInvoiceList(payload, this.solution, this.role, scriptAdaptor);
        	            
        	        		}else if(role === "PO Manager"){
        	        			dashboardscript.getAdvanceInvoiceList(payload, this.solution, this.role, scriptAdaptor);
        	            	}else if(role === "Warehouse Clerk"){
        	            		dashboardscript.getAdvanceInvoiceList(payload, this.solution, this.role, scriptAdaptor);
        	            	}else if(role === "Warehouse Manager"){
        	            		dashboardscript.getAdvanceInvoiceList(payload, this.solution, this.role, scriptAdaptor);
        	            	}else if(role === "GRN Clerk"){
        	            		dashboardscript.getAdvanceInvoiceList(payload, this.solution, this.role, scriptAdaptor);
        	            	}else if(role === "AP Clerk"){
        	            		dashboardscript.getinvoiceListforFinance(payload, this.solution, this.role,scriptAdaptor);
        	            	}else if(role === "Finance HOD"){
        	            		dashboardscript.getinvoiceListforFinance(payload, this.solution, this.role,scriptAdaptor);
        	            	}else if(role === "Internal Audit"){
        	            		dashboardscript.getinvoiceListforFinance(payload, this.solution, this.role,scriptAdaptor);
        	            	}else if(role === "Internal Audit BU"){
        	            		dashboardscript.getinvoiceListforFinance(payload, this.solution, this.role,scriptAdaptor);
        	            	}else if(role === "Finance Clerk HO"){
        	            		dashboardscript.getinvoiceListforFinance(payload, this.solution, this.role,scriptAdaptor);
        	            	}else if(role === "Internal Audit HO"){
        	            		dashboardscript.getinvoiceListforFinance(payload, this.solution, this.role,scriptAdaptor);
        	            	}else if(role === "Finance Clerk"){
        	            		dashboardscript.getinvoiceListforFinance(payload, this.solution, this.role,scriptAdaptor);
        	            	}
        	        		
        	        		
        	        		
        	        		/* Call the coordination completion method. */
        	        		complete();
        	        		});
        	        	// Callbacks to clean up when the user completes or
						// cancels the task creation
        	        	//METHOD WHICH IS CALLED WHEN WE CLICK ON CALCEL BUTTON AFTER COMPLETION CANCEL ACTION
        	        	coordination.participate(Constants.CoordTopic.AFTERCANCEL, function(context, complete, abort) {
        	        	if (responseself.editable) {
        	        	ControllerManager.unbind(responseself.editable);
        	        	delete responseself.editable;
        	        	delete responseself.propsController;
        	        	}
        	        	complete();
        	        	});
        	        	
        	        	//METHOD WHICH IS CALLED WHEN WE CLICK ON ANY DISPATCH BUTTON AFTER COMPLETION DISPATCH ACTION
        	        	coordination.participate(Constants.CoordTopic.AFTERCOMPLETE, function(context, complete, abort) {
        	        	if (responseself.editable) {
        	        	ControllerManager.unbind(responseself.editable);
        	        	delete responseself.editable;
        	        	delete responseself.propsController;
        	        	}
        	        	complete();
        	        	});
        	        	
        	        	
        	        	//METHOD WHICH IS CALLED WHEN WE CLICK ON ANY DISPATCH BUTTON BEFORE COMPLETION DISPATCH ACTION
        	        	coordination.participate(Constants.CoordTopic.BEFORECOMPLETE, function(context, complete, abort) {

        	        		
        	        		var theController = responseself.propsController;
        	        		
                                         var step=responseself.editable.icmWorkItem.stepName;
        	        		if(role === "Assigner"){
								
        	        			if(context[Constants.CoordContext.WKITEMRESPONSE] === "Complete"){
									
        	        				abort({"message":"You do not have access to complete the case!"});
        	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "REJECT"){
 
        	        				complete();        	        				
        	        				 
        	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "TERMINATE"){
        	        				complete();

        	        						 
        	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "RESUBMIT"){
        	        				
        	        			}else{
        	        				    complete();
        	        				 }
        	        			
        	        		}else if(role === "PO MANAGER"){
        	        			
        	        		}else if(role === "GM SPare Parts"){
        	        			
        	        		}else if(role === "GM FINANCE"){
        	        			
        	        		}else if(role === "WH CLERK"){
        	        			
        	        		}else if(role === "WH MANAGER"){
        	        			
        	        		}else if(role === "GRN CLERK"){
        	        			
        	        		}else if(role === "INVENTORY CLERK"){
        	        			
        	        		}else if(role === "AP ClERK"){
        	        			
        	        		}else if(role === "FINANCE HOD"){
        	        			
        	        		}else if(role === "INTERNAL AUDIT BU"){
        	        			
        	        		}else if(role === "FINANCE CLERK HO"){
        	        			
        	        		}else if(role === "INTERNAL AUDIT HO"){
        	        			
        	        		}else{
        	        			complete();
        	        		}
        	        		
        	        	
        	        	});
        	        	
        	        	
        	        	
        	        	//METHOD WHICH IS CALLED WHEN WE CLICK ON SAVE BUTTON
        	        	coordination.participate(Constants.CoordTopic.COMMIT, function(context, complete, abort) {
        	    
        	        
        	        		
        	        		var theController = responseself.propsController;
        	    
        	        		
        	        		var step=responseself.editable.icmWorkItem.stepName;
        	        		
	        				//changes made by nitin
	        				
							
							
        	        		if(role === "Assigner"){
	        			
	        			setCaseProperty(theController,"","LJ_AssignedBy","value",userId);
	        			
	        			complete();
						
        	        		}else if(role === "PO Clerk"){
        	        			
        	        			
        	        			
        	        			
        	        			
        	        			
        	        			if(context[Constants.CoordContext.WKITEMRESPONSE] === "Submit"){
 
        	        				
        	        				
        	        				   var standardpo=getCaseProperty(theController,"","IPS_StandardPONumber","value"); 		
        	        				
        	        			
        	        				   if(standardpo !==null && standardpo !==undefined && standardpo !=="")
        	        				   
        	        				   {
        	        				   
        	        				   var checkval = confirm("Are you sure you want to submit this case ?");
        	        				if(checkval){
        	        				
        	        				complete();
        	        				
        	        				}
        	        				else{
        	        					
        	        				abort({silent:true});
        	        				}}else{
        	        					
        	        					abort({"message":"Standard PO number cannot be empty!"});
        	        					
        	        				}
        	        				   
        	        				/*  var addCommentDialog = new AddCommentDialog({
                                          artifactType : "Case",
                                          artifactLabel : myStepName,
                                          commentContext :Constants.CommentContext.WORK_ITEM_COMPLETE,
                                          caseModel : myCase,
                                          workItem :self.workitemEdit.icmWorkItem,
                                          onClickClose : function() {
                                        	  
                                          },
        	        				     onClickOk : function(){
        	        				    	alert("ok"); 
        	        				    	 
        	        				     }
                                      });*/
        	        				
        	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "Return To PO Manager"){
 
        	        				 var checkval = confirm("Are you sure you want to return this case to PO Manager ?");
         	        				if(checkval){
         	        				
         	        				complete();
         	        				
         	        				}
         	        				else{
         	        					
         	        				abort({silent:true});
         	        				}     				
        	        				 
        	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "TERMINATE"){
        	        				complete();

        	        						 
        	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "RESUBMIT"){
        	        				
        	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "Register"){
        	        				 
       	        				 var checkval = confirm("Are you sure you want to register this case ?");
        	        				if(checkval){
        	        				
        	        				complete();
        	        				
        	        				}
        	        				else{
        	        					
        	        				abort({silent:true});
        	        				}     				
       	        				 
       	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "Save as Draft"){
        	        				 
       	        				 var checkval = confirm("Are you sure you want to save this case data ?");
        	        				if(checkval){
        	        				
        	        				complete();
        	        				
        	        				}
        	        				else{
        	        					
        	        				abort({silent:true});
        	        				}     				
       	        				 
       	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "Close"){
   	        				 
      	        				 var checkval = confirm("Are you sure you want to close this case?");
       	        				if(checkval){
       	        				
       	        				complete();
       	        				
       	        				}
       	        				else{
       	        					
       	        				abort({silent:true});
       	        				}     				
      	        				 
      	        			}else{
        	        				    complete();
        	        				 }
        	        			
        	        		}else if(role === "PO Manager"){
                           if (context[Constants.CoordContext.WKITEMRESPONSE] === "Approve"){
                        	   var checkval = confirm("Are you sure you want to approve this case ?");
    	        				if(checkval){
    	        				
    	        				complete();
    	        				
    	        				}
    	        				else{
    	        					
    	        				abort({silent:true});
    	        				}     			
        	        			}else if(context[Constants.CoordContext.WKITEMRESPONSE] === "Return To Sender"){
        	        				 var checkval = confirm("Are you sure you want to return this case  ?");
          	        				if(checkval){
          	        				
          	        				complete();
          	        				
          	        				}
          	        				else{
          	        					
          	        				abort({silent:true});
          	        				}     		
        	        			}else if(context[Constants.CoordContext.WKITEMRESPONSE] === "Terminate"){
        	        				 var checkval = confirm("Are you sure you want to terminate this case ?");
          	        				if(checkval){
          	        				
          	        				complete();
          	        				
          	        				}
          	        				else{
          	        					
          	        				abort({silent:true});
          	        				}     		
        	        			}else if(context[Constants.CoordContext.WKITEMRESPONSE] === "Save as Draft"){
       	        				 
          	        				 var checkval = confirm("Are you sure you want to save this case data ?");
           	        				if(checkval){
           	        				
           	        				complete();
           	        				
           	        				}
           	        				else{
           	        					
           	        				abort({silent:true});
           	        				}     				
          	        				 
          	        			}else if(context[Constants.CoordContext.WKITEMRESPONSE] === "Close"){
      	        				 
         	        				 var checkval = confirm("Are you sure you want to close this case?");
          	        				if(checkval){
          	        				
          	        				complete();
          	        				
          	        				}
          	        				else{
          	        					
          	        				abort({silent:true});
          	        				}}else{
        	        				complete();
        	        			}
        	        		}else if(role === "GM Spare Parts"){
        	        			if (context[Constants.CoordContext.WKITEMRESPONSE] === "Terminate"){
       	        				 var checkval = confirm("Are you sure you want to terminate this case ?");
          	        				if(checkval){
          	        				
          	        				complete();
          	        				
          	        				}
          	        				else{
          	        					
          	        				abort({silent:true});
          	        				}    

       	        						 
       	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "Approve"){
       	        				var checkval = confirm("Are you sure you want to submit this case ?");
       	        				if(checkval){
       	        				
       	        				complete();
       	        				
       	        				}
       	        				else{
       	        					
       	        				abort({silent:true});
       	        				}

       	        						 
       	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "Return To Sender"){
       	        				var checkval = confirm("Are you sure you want to return this case ?");
       	        				if(checkval){
       	        				
       	        				complete();
       	        				
       	        				}
       	        				else{
       	        					
       	        				abort({silent:true});
       	        				}

       	        						 
       	        			}else{
       	        				complete();
       	        			}
       	        		}else if(role === "GM FINANCE"){
        	        			
        	        		}else if(role === "Warehouse Clerk"){
        	        		
        	        			if (context[Constants.CoordContext.WKITEMRESPONSE] === "Add"){
       	        				 var checkval = confirm("Are you sure you want to Add this case ?");
          	        				if(checkval){
          	        				
          	        				complete();
          	        				
          	        				}
          	        				else{
          	        					
          	        				abort({silent:true});
          	        				}    

       	        						 
       	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "Cancel"){
      	        				 var checkval = confirm("Are you sure you want to Add this case ?");
       	        				if(checkval){
       	        				
       	        				complete();
       	        				
       	        				}
       	        				else{
       	        					
       	        				abort({silent:true});
       	        				}    

    	        						 
    	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "Return To WH Manager"){
     	        				 var checkval = confirm("Are you sure you want to return this case ?");
        	        				if(checkval){
        	        				
        	        				complete();
        	        				
        	        				}
        	        				else{
        	        					
        	        				abort({silent:true});
        	        				}    

     	        						 
     	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "Terminate"){
        	        				 var checkval = confirm("Are you sure you want to terminate this case ?");
           	        				if(checkval){
           	        				
           	        				complete();
           	        				
           	        				}
           	        				else{
           	        					
           	        				abort({silent:true});
           	        				}    

        	        						 
        	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "Submit"){
        	        				var checkval = confirm("Are you sure you want to submit this case ?");
        	        				if(checkval){
        	        				
        	        				complete();
        	        				
        	        				}
        	        				else{
        	        					
        	        				abort({silent:true});
        	        				}

        	        						 
        	        			}else{
        	        				complete();
        	        			}
        	        		}else if(role === "Warehouse Manager"){
                                if (context[Constants.CoordContext.WKITEMRESPONSE] === "Approve"){
                             	   var checkval = confirm("Are you sure you want to approve this case ?");
         	        				if(checkval){
         	        				
         	        				complete();
         	        				
         	        				}
         	        				else{
         	        					
         	        				abort({silent:true});
         	        				}     			
             	        			}else if(context[Constants.CoordContext.WKITEMRESPONSE] === "Return To Sender"){
             	        				 var checkval = confirm("Are you sure you want to return this case  ?");
               	        				if(checkval){
               	        				
               	        				complete();
               	        				
               	        				}
               	        				else{
               	        					
               	        				abort({silent:true});
               	        				}     		
             	        			}else if(context[Constants.CoordContext.WKITEMRESPONSE] === "Terminate"){
             	        				 var checkval = confirm("Are you sure you want to terminate this case ?");
               	        				if(checkval){
               	        				
               	        				complete();
               	        				
               	        				}
               	        				else{
               	        					
               	        				abort({silent:true});
               	        				}     		
             	        			}else{
             	        				complete();
             	        			}
             	        		}else if(role === "GRN Clerk"){

                                    if (context[Constants.CoordContext.WKITEMRESPONSE] === "Fetch GRN"){
                                 	   var checkval = confirm("Are you sure you want to Fetch GRN for this case ?");
             	        				if(checkval){
             	        				
             	        				complete();
             	        				
             	        				}
             	        				else{
             	        					
             	        				abort({silent:true});
             	        				}     			
                 	        			}else if(context[Constants.CoordContext.WKITEMRESPONSE] === "Return To WH"){
                 	        				 var checkval = confirm("Are you sure you want to return this case  ?");
                   	        				if(checkval){
                   	        				
                   	        				complete();
                   	        				
                   	        				}
                   	        				else{
                   	        					
                   	        				abort({silent:true});
                   	        				}     		
                 	        			}else{
                 	        				complete();
                 	        			}
                 	        		
        	        			
        	        		}else if(role === "INVENTORY CLERK"){
        	        			
        	        		}else if(role === "AP Clerk"){
        	        			if (context[Constants.CoordContext.WKITEMRESPONSE] === "Submit"){
                                	   var checkval = confirm("Are you sure you want to submit this case ?");
            	        				if(checkval){
            	        				
            	        				complete();
            	        				
            	        				}
            	        				else{
            	        					
            	        				abort({silent:true});
            	        				}     			
                	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "Approve"){
                               	   var checkval = confirm("Are you sure you want to approve this case ?");
           	        				if(checkval){
           	        				
           	        				complete();
           	        				
           	        				}
           	        				else{
           	        					
           	        				abort({silent:true});
           	        				}     			
               	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "Complete"){
                              	   var checkval = confirm("Are you sure you want to complete this case ?");
          	        				if(checkval){
          	        				
          	        				complete();
          	        				
          	        				}
          	        				else{
          	        					
          	        				abort({silent:true});
          	        				}     			
              	        			}else if(context[Constants.CoordContext.WKITEMRESPONSE] === "Return To Sender"){
              	        				 var checkval = confirm("Are you sure you want to return this case  ?");
                	        				if(checkval){
                	        				
                	        				complete();
                	        				
                	        				}
                	        				else{
                	        					
                	        				abort({silent:true});
                	        				}     		
              	        			}else if(context[Constants.CoordContext.WKITEMRESPONSE] === "Terminate"){
              	        				 var checkval = confirm("Are you sure you want to terminate this case ?");
                	        				if(checkval){
                	        				
                	        				complete();
                	        				
                	        				}
                	        				else{
                	        					
                	        				abort({silent:true});
                	        				}     		
              	        			}else{
              	        				complete();
              	        			}
              	        		}else if(role === "Finance HOD"){
            	        			if (context[Constants.CoordContext.WKITEMRESPONSE] === "Submit"){
                                 	   var checkval = confirm("Are you sure you want to submit this case ?");
             	        				if(checkval){
             	        				
             	        				complete();
             	        				
             	        				}
             	        				else{
             	        					
             	        				abort({silent:true});
             	        				}     			
                 	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "Approve"){
                                	   var checkval = confirm("Are you sure you want to approve this case ?");
            	        				if(checkval){
            	        				
            	        				complete();
            	        				
            	        				}
            	        				else{
            	        					
            	        				abort({silent:true});
            	        				}     			
                	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "Complete"){
                               	   var checkval = confirm("Are you sure you want to complete this case ?");
           	        				if(checkval){
           	        				
           	        				complete();
           	        				
           	        				}
           	        				else{
           	        					
           	        				abort({silent:true});
           	        				}     			
               	        			}else if(context[Constants.CoordContext.WKITEMRESPONSE] === "Return To Sender"){
               	        				 var checkval = confirm("Are you sure you want to return this case  ?");
                 	        				if(checkval){
                 	        				
                 	        				complete();
                 	        				
                 	        				}
                 	        				else{
                 	        					
                 	        				abort({silent:true});
                 	        				}     		
               	        			}else if(context[Constants.CoordContext.WKITEMRESPONSE] === "Terminate"){
               	        				 var checkval = confirm("Are you sure you want to terminate this case ?");
                 	        				if(checkval){
                 	        				
                 	        				complete();
                 	        				
                 	        				}
                 	        				else{
                 	        					
                 	        				abort({silent:true});
                 	        				}     		
               	        			}else{
               	        				complete();
               	        			}
        	        			
        	        		}else if(role === "Finance Clerk HO"){
        	        			if (context[Constants.CoordContext.WKITEMRESPONSE] === "Submit"){
                             	   var checkval = confirm("Are you sure you want to submit this case ?");
         	        				if(checkval){
         	        				
         	        				complete();
         	        				
         	        				}
         	        				else{
         	        					
         	        				abort({silent:true});
         	        				}     			
             	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "Approve"){
                            	   var checkval = confirm("Are you sure you want to approve this case ?");
        	        				if(checkval){
        	        				
        	        				complete();
        	        				
        	        				}
        	        				else{
        	        					
        	        				abort({silent:true});
        	        				}     			
            	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "Complete"){
                           	   var checkval = confirm("Are you sure you want to complete this case ?");
       	        				if(checkval){
       	        				
       	        				complete();
       	        				
       	        				}
       	        				else{
       	        					
       	        				abort({silent:true});
       	        				}     			
           	        			}else if(context[Constants.CoordContext.WKITEMRESPONSE] === "Return To Sender"){
           	        				 var checkval = confirm("Are you sure you want to return this case  ?");
             	        				if(checkval){
             	        				
             	        				complete();
             	        				
             	        				}
             	        				else{
             	        					
             	        				abort({silent:true});
             	        				}     		
           	        			}else if(context[Constants.CoordContext.WKITEMRESPONSE] === "Terminate"){
           	        				 var checkval = confirm("Are you sure you want to terminate this case ?");
             	        				if(checkval){
             	        				
             	        				complete();
             	        				
             	        				}
             	        				else{
             	        					
             	        				abort({silent:true});
             	        				}     		
           	        			}else{
           	        				complete();
           	        			}
           	        		}else if(role === "Internal Audit HO"){

        	        			if (context[Constants.CoordContext.WKITEMRESPONSE] === "Submit"){
                             	   var checkval = confirm("Are you sure you want to submit this case ?");
         	        				if(checkval){
         	        				
         	        				complete();
         	        				
         	        				}
         	        				else{
         	        					
         	        				abort({silent:true});
         	        				}     			
             	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "Approve"){
                            	   var checkval = confirm("Are you sure you want to approve this case ?");
        	        				if(checkval){
        	        				
        	        				complete();
        	        				
        	        				}
        	        				else{
        	        					
        	        				abort({silent:true});
        	        				}     			
            	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "Complete"){
                           	   var checkval = confirm("Are you sure you want to complete this case ?");
       	        				if(checkval){
       	        				
       	        				complete();
       	        				
       	        				}
       	        				else{
       	        					
       	        				abort({silent:true});
       	        				}     			
           	        			}else if(context[Constants.CoordContext.WKITEMRESPONSE] === "Return To Sender"){
           	        				 var checkval = confirm("Are you sure you want to return this case  ?");
             	        				if(checkval){
             	        				
             	        				complete();
             	        				
             	        				}
             	        				else{
             	        					
             	        				abort({silent:true});
             	        				}     		
           	        			}else if(context[Constants.CoordContext.WKITEMRESPONSE] === "Terminate"){
           	        				 var checkval = confirm("Are you sure you want to terminate this case ?");
             	        				if(checkval){
             	        				
             	        				complete();
             	        				
             	        				}
             	        				else{
             	        					
             	        				abort({silent:true});
             	        				}     		
           	        			}else{
           	        				complete();
           	        			}
           	        		
           	        			
           	        			
           	        		}else if(role === "Internal Audit BU"){
        	        			if (context[Constants.CoordContext.WKITEMRESPONSE] === "Submit"){
                             	   var checkval = confirm("Are you sure you want to submit this case ?");
         	        				if(checkval){
         	        				
         	        				complete();
         	        				
         	        				}
         	        				else{
         	        					
         	        				abort({silent:true});
         	        				}     			
             	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "Approve"){
                            	   var checkval = confirm("Are you sure you want to approve this case ?");
        	        				if(checkval){
        	        				
        	        				complete();
        	        				
        	        				}
        	        				else{
        	        					
        	        				abort({silent:true});
        	        				}     			
            	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "Complete"){
                           	   var checkval = confirm("Are you sure you want to complete this case ?");
       	        				if(checkval){
       	        				
       	        				complete();
       	        				
       	        				}
       	        				else{
       	        					
       	        				abort({silent:true});
       	        				}     			
           	        			}else if(context[Constants.CoordContext.WKITEMRESPONSE] === "Return To Sender"){
           	        				 var checkval = confirm("Are you sure you want to return this case  ?");
             	        				if(checkval){
             	        				
             	        				complete();
             	        				
             	        				}
             	        				else{
             	        					
             	        				abort({silent:true});
             	        				}     		
           	        			}else if(context[Constants.CoordContext.WKITEMRESPONSE] === "Terminate"){
           	        				 var checkval = confirm("Are you sure you want to terminate this case ?");
             	        				if(checkval){
             	        				
             	        				complete();
             	        				
             	        				}
             	        				else{
             	        					
             	        				abort({silent:true});
             	        				}     		
           	        			}else{
           	        				complete();
           	        			}
           	        		}else{
        	        			complete();
        	        		}
        	        		
        	        	});		 
        	        		
        	        	
        		
        	        });
        	 
        	        return payload;
        	 
        	}
        	catch (exception) {
        	    alert(exception);
        	}
        	
        },
        
        advancedecisionaction: function(payload, solution, role, scriptAdaptor){
        	try {
        		
        		
        		
        	    var responseself = scriptAdaptor;
        	    console.log("***** responseself *****");
        	    console.log(responseself);
        	    var coordination = payload.coordination;
        	        responseself.editable = payload.workItemEditable;
        	        var solution = responseself.solution;
        	        var prefix = solution.getPrefix();
        	        var userName = ecm.model.desktop.userDisplayName;
        	        var userId = ecm.model.desktop.userId;
        	        var role = ecm.model.desktop.currentRole.name;
        	        
        	        removeResponseButtons(role,responseself.editable);
        	        
        	        require(["icm/base/Constants","icm/model/properties/controller/ControllerManager"], function(Constants, ControllerManager){
        	        	
        	        	//METHOD WHICH IS CALLED BEFORE LOADING WIDGET IS FINISHED
        	        	coordination.participate(Constants.CoordTopic.BEFORELOADWIDGET, function(context, complete, abort) {
        	        		/* Obtain the controller binding for the editable. */
        	        		if(!responseself.hasOwnProperty('propsController')){
        	        		responseself.propsController = ControllerManager.bind(responseself.editable);
        	        		 var step=responseself.editable.icmWorkItem.stepName;
        	        		
        	            	var theController=responseself.propsController;
        	            
        	        		
        	        		if(role === "PO Clerk"){
        	            		beforeloadaction.beforeloadactionPO(theController,payload, this.solution, this.role, scriptAdaptor);
        	            	}else if(role === "PO Manager"){
        	            		beforeloadaction.beforeloadactionPM(theController,payload, this.solution, this.role, scriptAdaptor);
        	        			
        	        		}else if(role === "GM SPARE PARTS"){
        	        			beforeloadaction.beforeloadactionGSP(theController,payload, this.solution, this.role, scriptAdaptor);
        	        			
        	        		}else if(role === "GM FINANCE"){
        	        			beforeloadaction.beforeloadactionGMF(theController,payload, this.solution, this.role, scriptAdaptor);
        	        			
        	        		}else if(role === "WH CLERK"){
        	        			beforeloadaction.beforeloadactionWHC(theController,payload, this.solution, this.role, scriptAdaptor);
        	        		}else if(role === "WH MANAGER"){
        	        			beforeloadaction.beforeloadactionWHM(theController,payload, this.solution, this.role, scriptAdaptor);
        	        		}else if(role === "GRN CLERK"){
        	        			beforeloadaction.beforeloadactionGRC(theController,payload, this.solution, this.role, scriptAdaptor);
        	        			
        	        		}else if(role === "INVENTORY CLERK"){
        	        			beforeloadaction.beforeloadactionIC(theController,payload, this.solution, this.role, scriptAdaptor);
        	        		}else if(role === "AP Clerk"){
        	        			beforeloadaction.beforeloadactionAPC(theController,payload, this.solution, this.role, scriptAdaptor);
        	        			
        	        		}else if(role === "Finance HOD"){
        	        			beforeloadaction.beforeloadactionFHO(theController,payload, this.solution, this.role, scriptAdaptor);
        	        			
        	        		}else if(role === "Internal Audit"){
        	        			beforeloadaction.beforeloadactionIAB(theController,payload, this.solution, this.role, scriptAdaptor);
        	        			
        	        		}else if(role === "Internal Audit BU"){
        	        			beforeloadaction.beforeloadactionIAB(theController,payload, this.solution, this.role, scriptAdaptor);
        	        			
        	        		}else if(role === "Finance Clerk HO"){
        	        			beforeloadaction.beforeloadactionFCH(theController,payload, this.solution, this.role, scriptAdaptor);
        	        			
        	        		}else if(role === "Internal Audit BU"){
        	        			beforeloadaction.beforeloadactionIAH(theController,payload, this.solution, this.role, scriptAdaptor);
        	        			
        	        		}else if(role === "Internal Audit HO"){
        	        			beforeloadaction.beforeloadactionIAH(theController,payload, this.solution, this.role, scriptAdaptor);
            	        		
        	        			
        	        		}else if(role === "Finance Clerk"){
        	        			beforeloadaction.beforeloadactionFCH(theController,payload, this.solution, this.role, scriptAdaptor);
        	        			
        	        		}
        	        		
        	        		}
        	        		/* Call the coordination completion method. */
        	        		complete();
        	        		});
        	        	
        	        	
        	        	//METHOD WHICH IS CALLED WHEN LOADING WIDGET IS FINISHED
        	        	coordination.participate(Constants.CoordTopic.AFTERLOADWIDGET, function(context, complete, abort) {
        	        		/* Obtain the controller binding for the editable. */
        	        		
        	        		
        	        		 /*if(payload.caseEditable != undefined){
        	        				caseEdit=payload.caseEditable;
        	        				page="addCase";
        	        			}else if(payload.workItemEditable){
        	        				caseEdit=payload.workItemEditable;
        	        				page="workDetail";
        	        			}*/
        	        		
        	        	/*	
        	        			
        	        		var coord = payload.coordination;
        	        		var solution = this.solution;
        	        		var prefix = "IPS";
//alert("before req");
        	        		

        	        		if(coord){
//alert("1");
        	        		//coord.participate(Constants.CoordTopic.LOADWIDGET, function(context, complete, abort) {
  //      	alert("2");        		        
        	        		        if (payload.workItemEditable.icmWorkItem.ecmWorkItem.attributes.hasOwnProperty("IPS_Status")){
    //    	        	alert("3");
        	        		        	var automated = payload.workItemEditable.icmWorkItem.ecmWorkItem.attributes["IPS_Status"];
        	        		                alert(automated);                
        	        		                if(automated){
        	        		                                          
        	        		                 var nodeSearch = dojo.query('.icmCaseSearch icmPageWidget');

        	        		                                 
        	        		                        //         domStyle.set(nodeSearch[0].domNode, 'display', 'none');
        	        		                                }
        	        		                
        	        		                                        
        	        		                                                         }
        	        		                                                         
        	        		                                                         

        	        		  complete();
        	        		// });
        	        		}*/
        	        		
        	        		
        	        		
        	        		
        	        		
        	        		
        	        		if(!responseself.hasOwnProperty('propsController')){
        	        		responseself.propsController = ControllerManager.bind(responseself.editable);
        	        		
        	        		}
        	        	
        	        		
        	        		if(role === "PO Clerk"){

        	        			dashboardscript.getAdvanceInvoiceList(payload, this.solution, this.role, scriptAdaptor);
        	            
        	        		}else if(role === "PO Manager"){
        	        			dashboardscript.getAdvanceInvoiceList(payload, this.solution, this.role, scriptAdaptor);
        	            	}else if(role === "Warehouse Clerk"){
        	            		dashboardscript.getAdvanceInvoiceList(payload, this.solution, this.role, scriptAdaptor);
        	            	}else if(role === "Warehouse Manager"){
        	            		dashboardscript.getAdvanceInvoiceList(payload, this.solution, this.role, scriptAdaptor);
        	            	}else if(role === "GRN Clerk"){
        	            		dashboardscript.getAdvanceInvoiceList(payload, this.solution, this.role, scriptAdaptor);
        	            	}else if(role === "AP Clerk"){
        	            		dashboardscript.getinvoiceListforFinance(payload, this.solution, this.role,scriptAdaptor);
        	            	}else if(role === "Finance HOD"){
        	            		dashboardscript.getinvoiceListforFinance(payload, this.solution, this.role,scriptAdaptor);
        	            	}else if(role === "Internal Audit"){
        	            		dashboardscript.getinvoiceListforFinance(payload, this.solution, this.role,scriptAdaptor);
        	            	}else if(role === "Internal Audit BU"){
        	            		dashboardscript.getinvoiceListforFinance(payload, this.solution, this.role,scriptAdaptor);
        	            	}else if(role === "Finance Clerk HO"){
        	            		dashboardscript.getinvoiceListforFinance(payload, this.solution, this.role,scriptAdaptor);
        	            	}else if(role === "Internal Audit HO"){
        	            		dashboardscript.getinvoiceListforFinance(payload, this.solution, this.role,scriptAdaptor);
        	            	}else if(role === "Finance Clerk"){
        	            		dashboardscript.getinvoiceListforFinance(payload, this.solution, this.role,scriptAdaptor);
        	            	}
        	        		
        	        		
        	        		
        	        		/* Call the coordination completion method. */
        	        		complete();
        	        		});
        	        	// Callbacks to clean up when the user completes or
						// cancels the task creation
        	        	//METHOD WHICH IS CALLED WHEN WE CLICK ON CALCEL BUTTON AFTER COMPLETION CANCEL ACTION
        	        	coordination.participate(Constants.CoordTopic.AFTERCANCEL, function(context, complete, abort) {
        	        	if (responseself.editable) {
        	        	ControllerManager.unbind(responseself.editable);
        	        	delete responseself.editable;
        	        	delete responseself.propsController;
        	        	}
        	        	complete();
        	        	});
        	        	
        	        	//METHOD WHICH IS CALLED WHEN WE CLICK ON ANY DISPATCH BUTTON AFTER COMPLETION DISPATCH ACTION
        	        	coordination.participate(Constants.CoordTopic.AFTERCOMPLETE, function(context, complete, abort) {
        	        	if (responseself.editable) {
        	        	ControllerManager.unbind(responseself.editable);
        	        	delete responseself.editable;
        	        	delete responseself.propsController;
        	        	}
        	        	complete();
        	        	});
        	        	
        	        	
        	        	//METHOD WHICH IS CALLED WHEN WE CLICK ON ANY DISPATCH BUTTON BEFORE COMPLETION DISPATCH ACTION
        	        	coordination.participate(Constants.CoordTopic.BEFORECOMPLETE, function(context, complete, abort) {

        	        		
        	        		var theController = responseself.propsController;
        	        		
        	        		if(role === "PO Clerk"){
        	        			if(context[Constants.CoordContext.WKITEMRESPONSE] === "SUBMIT"){
        	        				
        	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "REJECT"){
 
        	        				complete();        	        				
        	        				 
        	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "TERMINATE"){
        	        				complete();

        	        						 
        	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "RESUBMIT"){
        	        				
        	        			}else{
        	        				    complete();
        	        				 }
        	        			
        	        		}else if(role === "PO Manager"){
        	        			complete();
        	        		}else if(role === "GM Spare Parts"){
        	        			complete();
        	        		}else if(role === "GM Finance")
        	        			{
        	        			complete();
        	        			}else if(role === "WH CLERK"){
        	        			
        	        		}else if(role === "WH MANAGER"){
        	        			
        	        		}else if(role === "GRN CLERK"){
        	        			
        	        		}else if(role === "INVENTORY CLERK"){
        	        			
        	        		}else if(role === "AP Clerk"){
        	        			complete();
        	        		}else if(role === "FINANCE HOD"){
        	        			
        	        		}else if(role === "INTERNAL AUDIT BU"){
        	        			
        	        		}else if(role === "FINANCE CLERK HO"){
        	        			
        	        		}else if(role === "INTERNAL AUDIT HO"){
        	        			
        	        		}else{
        	        			
        	        		}
        	        		
        	        	
        	        	});
        	        	
        	        	
        	        	
        	        	//METHOD WHICH IS CALLED WHEN WE CLICK ON SAVE BUTTON
        	        	coordination.participate(Constants.CoordTopic.COMMIT, function(context, complete, abort) {
        	    
        	        
        	        		
        	        		var theController = responseself.propsController;
        	    
        	        		
        	        		var step=responseself.editable.icmWorkItem.stepName;
        	        		
	        				//changes made by nitin
	        				
        	        		if(role === "PO Clerk"){
        	        			
        	        			
        	        			
        	        			
        	        			
        	        			
        	        			if(context[Constants.CoordContext.WKITEMRESPONSE] === "Submit"){
 
        	        				
        	        				
        	        				   var standardpo=getCaseProperty(theController,"","IPS_StandardPONumber","value"); 		
        	        				
        	        			
        	        				   if(standardpo !==null && standardpo !==undefined && standardpo !=="")
        	        				   
        	        				   {
        	        				   
        	        				   var checkval = confirm("Are you sure you want to submit this case ?");
        	        				if(checkval){
        	        				
        	        				complete();
        	        				
        	        				}
        	        				else{
        	        					
        	        				abort({silent:true});
        	        				}}else{
        	        					
        	        					abort({"message":"Standard PO number cannot be empty!"});
        	        					
        	        				}
        	        			
        	        				
        	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "Return To PO Manager"){
 
        	        				 var checkval = confirm("Are you sure you want to return this case to PO Manager ?");
         	        				if(checkval){
         	        				
         	        				complete();
         	        				
         	        				}
         	        				else{
         	        					
         	        				abort({silent:true});
         	        				}     				
        	        				 
        	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "TERMINATE"){
        	        				complete();

        	        						 
        	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "RESUBMIT"){
        	        				
        	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "Save as Draft"){
        	        				 
       	        				 var checkval = confirm("Are you sure you want to save this case data ?");
        	        				if(checkval){
        	        				
        	        				complete();
        	        				
        	        				}
        	        				else{
        	        					
        	        				abort({silent:true});
        	        				}     				
       	        				 
       	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "Close"){
   	        				 
      	        				 var checkval = confirm("Are you sure you want to close this case?");
       	        				if(checkval){
       	        				
       	        				complete();
       	        				
       	        				}
       	        				else{
       	        					
       	        				abort({silent:true});
       	        				}     				
      	        				 
      	        			}else{
        	        				    complete();
        	        				 }
        	        			
        	        		}else if(role === "PO Manager"){
                           if (context[Constants.CoordContext.WKITEMRESPONSE] === "Approve"){
                        	   var checkval = confirm("Are you sure you want to approve this case ?");
    	        				if(checkval){
    	        				
    	        				complete();
    	        				
    	        				}
    	        				else{
    	        					
    	        				abort({silent:true});
    	        				}     			
        	        			}else if(context[Constants.CoordContext.WKITEMRESPONSE] === "Return To Sender"){
        	        				 var checkval = confirm("Are you sure you want to return this case  ?");
          	        				if(checkval){
          	        				
          	        				complete();
          	        				
          	        				}
          	        				else{
          	        					
          	        				abort({silent:true});
          	        				}     		
        	        			}else if(context[Constants.CoordContext.WKITEMRESPONSE] === "Terminate"){
        	        				 var checkval = confirm("Are you sure you want to terminate this case ?");
          	        				if(checkval){
          	        				
          	        				complete();
          	        				
          	        				}
          	        				else{
          	        					
          	        				abort({silent:true});
          	        				}     		
        	        			}else if(context[Constants.CoordContext.WKITEMRESPONSE] === "Save as Draft"){
       	        				 
          	        				 var checkval = confirm("Are you sure you want to save this case data ?");
           	        				if(checkval){
           	        				
           	        				complete();
           	        				
           	        				}
           	        				else{
           	        					
           	        				abort({silent:true});
           	        				}     				
          	        				 
          	        			}else if(context[Constants.CoordContext.WKITEMRESPONSE] === "Close"){
      	        				 
         	        				 var checkval = confirm("Are you sure you want to close this case?");
          	        				if(checkval){
          	        				
          	        				complete();
          	        				
          	        				}
          	        				else{
          	        					
          	        				abort({silent:true});
          	        				}}else{
        	        				complete();
        	        			}
        	        		}else if(role === "GM Spare Parts"){
        	        			if (context[Constants.CoordContext.WKITEMRESPONSE] === "Terminate"){
       	        				 var checkval = confirm("Are you sure you want to terminate this case ?");
          	        				if(checkval){
          	        				
          	        				complete();
          	        				
          	        				}
          	        				else{
          	        					
          	        				abort({silent:true});
          	        				}    

       	        						 
       	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "Approve"){
       	        				var checkval = confirm("Are you sure you want to submit this case ?");
       	        				if(checkval){
       	        				
       	        				complete();
       	        				
       	        				}
       	        				else{
       	        					
       	        				abort({silent:true});
       	        				}

       	        						 
       	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "Return To Sender"){
       	        				var checkval = confirm("Are you sure you want to return this case ?");
       	        				if(checkval){
       	        				
       	        				complete();
       	        				
       	        				}
       	        				else{
       	        					
       	        				abort({silent:true});
       	        				}

       	        						 
       	        			}else{
       	        				complete();
       	        			}
       	        		}else if(role === "GM Finance"){
    	        			if (context[Constants.CoordContext.WKITEMRESPONSE] === "Terminate"){
      	        				 var checkval = confirm("Are you sure you want to terminate this case ?");
         	        				if(checkval){
         	        				
         	        				complete();
         	        				
         	        				}
         	        				else{
         	        					
         	        				abort({silent:true});
         	        				}    

      	        						 
      	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "Approve"){
      	        				var checkval = confirm("Are you sure you want to submit this case ?");
      	        				if(checkval){
      	        				
      	        				complete();
      	        				
      	        				}
      	        				else{
      	        					
      	        				abort({silent:true});
      	        				}

      	        						 
      	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "Return To Sender"){
      	        				var checkval = confirm("Are you sure you want to return this case ?");
      	        				if(checkval){
      	        				
      	        				complete();
      	        				
      	        				}
      	        				else{
      	        					
      	        				abort({silent:true});
      	        				}

      	        						 
      	        			}else{
      	        				complete();
      	        			}
      	        		}else if(role === "Warehouse Clerk"){
        	        			if (context[Constants.CoordContext.WKITEMRESPONSE] === "Terminate"){
        	        				 var checkval = confirm("Are you sure you want to terminate this case ?");
           	        				if(checkval){
           	        				
           	        				complete();
           	        				
           	        				}
           	        				else{
           	        					
           	        				abort({silent:true});
           	        				}    

        	        						 
        	        			}else if (context[Constants.CoordContext.WKITEMRESPONSE] === "Submit"){
        	        				var checkval = confirm("Are you sure you want to submit this case ?");
        	        				if(checkval){
        	        				
        	        				complete();
        	        				
        	        				}
        	        				else{
        	        					
        	        				abort({silent:true});
        	        				}

        	        						 
        	        			}else{
        	        				complete();
        	        			}
        	        		}else if(role === "Warehouse Manager"){
                                if (context[Constants.CoordContext.WKITEMRESPONSE] === "Approve"){
                             	   var checkval = confirm("Are you sure you want to approve this case ?");
         	        				if(checkval){
         	        				
         	        				complete();
         	        				
         	        				}
         	        				else{
         	        					
         	        				abort({silent:true});
         	        				}     			
             	        			}else if(context[Constants.CoordContext.WKITEMRESPONSE] === "Return To Sender"){
             	        				 var checkval = confirm("Are you sure you want to return this case  ?");
               	        				if(checkval){
               	        				
               	        				complete();
               	        				
               	        				}
               	        				else{
               	        					
               	        				abort({silent:true});
               	        				}     		
             	        			}else if(context[Constants.CoordContext.WKITEMRESPONSE] === "Terminate"){
             	        				 var checkval = confirm("Are you sure you want to terminate this case ?");
               	        				if(checkval){
               	        				
               	        				complete();
               	        				
               	        				}
               	        				else{
               	        					
               	        				abort({silent:true});
               	        				}     		
             	        			}else{
             	        				complete();
             	        			}
             	        		}else if(role === "GRN Clerk"){

                                    if (context[Constants.CoordContext.WKITEMRESPONSE] === "Fetch GRN"){
                                 	   var checkval = confirm("Are you sure you want to Fetch GRN for this case ?");
             	        				if(checkval){
             	        				
             	        				complete();
             	        				
             	        				}
             	        				else{
             	        					
             	        				abort({silent:true});
             	        				}     			
                 	        			}else if(context[Constants.CoordContext.WKITEMRESPONSE] === "Return To WH"){
                 	        				 var checkval = confirm("Are you sure you want to return this case  ?");
                   	        				if(checkval){
                   	        				
                   	        				complete();
                   	        				
                   	        				}
                   	        				else{
                   	        					
                   	        				abort({silent:true});
                   	        				}     		
                 	        			}else{
                 	        				complete();
                 	        			}
                 	        		
        	        			
        	        		}else if(role === "INVENTORY CLERK"){
        	        			
        	        		}else if(role === "AP Clerk"){
        	        			if(context[Constants.CoordContext.WKITEMRESPONSE] === "Complete Process"){
        	        				 var checkval = confirm("Are you sure you want to complete this case  ?");
          	        				if(checkval){
          	        				
          	        				complete();
          	        				
          	        				}
          	        				else{
          	        					
          	        				abort({silent:true});
          	        				}     		
        	        			}else{
        	        				complete();
        	        			}
        	        		}else if(role === "FINANCE HOD"){
        	        			
        	        		}else if(role === "INTERNAL AUDIT BU"){
        	        			
        	        		}else if(role === "FINANCE CLERK HO"){
        	        			
        	        		}else if(role === "INTERNAL AUDIT HO"){
        	        			
        	        		}else{
        	        			complete();
        	        		}
        	        		
        	        	});		 
        	        		
        	        	
        		
        	        });
        	 
        	        return payload;
        	 
        	}
        	catch (exception) {
        	    alert(exception);
        	}
        	
        },
        
        propupdateaction: function(payload, solution, role, scriptAdaptor){
        	
        	var rolename=role.name;
        	       	/*   var responseself = scriptAdaptor;
        	   responseself.editable = payload.workItemEditable;
        	   responseself.propsController = ControllerManager.bind(responseself.editable);
      		 var step=responseself.editable.icmWorkItem.stepName;*/
      		
         
        	var workItemEditable = self.context.model.workItemEditable;
        	   var propertyCollection = workItemEditable._getPropertiesCollection();

        
        	  var  theController = ControllerManager.bind(workItemEditable);
        	    console.log('theController is ' + theController);
        
        	
        	
        	
        	
        	   //var theController=scriptAdaptor.propsController;
        	/*var myWorkItemEditable = payload.Case;
        	var control=ControllerManager.bind(myWorkItemEditable);
        	*/

   		// var step=responseself.editable.icmWorkItem.stepName;
   		
  //     	var theController=responseself.propsController;
        	
        	 if(rolename === "Branch Checker"){
        		propupdateaction.propupdateactionBC(theController,payload, this.solution, this.role, scriptAdaptor);
        	}
        	 else if(rolename === "Assigner"){
        		propupdateaction.propupdateactionAO(theController,payload, this.solution, this.role, scriptAdaptor);
        	}
        else{
    			
    		}
    			
     	
        	
        },
        
        //changes made by nitin for production issue no 4 on 12 March 2018
        rowdeletedaction: function(payload, solution, role, scriptAdaptor){
        	var rolename=role.name;
        	var theController=scriptAdaptor.propsController;
        	if(rolename === "Branch Scrutiniser" || rolename === "Internal Agent" || rolename === "External Agent" || rolename === "Branch Officer" || rolename === "Sales Officer"){
        		propupdateaction.rowdeletion(theController,payload, this.solution, this.role, scriptAdaptor);
    		}
        	
        }//changes made by nitin for production issue no 4 on 12 March 2018
        
        
        
	});
    
   
  
    function setCaseProperty(propcontroller,context,propname,settype,setvalue){
    	try{
    	 if(context ==="F_CaseFolder"){
    		propcontroller.getPropertyController("F_CaseFolder",propname).set(settype,setvalue);
    	}else if(context ==="F_CaseTask"){
    		propcontroller.getPropertyController("F_CaseTask",propname).set(settype,setvalue);
    	}else if(context ==="F_WorkflowField"){
    		propcontroller.getPropertyController("F_WorkflowField",propname).set(settype,setvalue);
    	}else{
    		propcontroller.getPropertyController(propname).set(settype,setvalue);
    	}
    	 return true;
    	}catch (Error) {
    		console.log("Field Update failed -"+propname);
	        alert ("Source Module: Response Action Adaptor\r\n\r\n"+Error.name+" -"+Error.description+"\r\n"+Error.message +" for -"+propname);
	    return false;
	}
    	
    }
   
    function getCaseProperty(propcontroller,context,propname,gettype){
    	try{	
    var caseprop=null;	
   	 if(context ==="F_CaseFolder"){
   		caseprop= propcontroller.getPropertyController("F_CaseFolder",propname).get(gettype);
   	}else if(context ==="F_CaseTask"){
   		caseprop=propcontroller.getPropertyController("F_CaseTask",propname).get(gettype);
   	}else if(context ==="F_WorkflowField"){
   		caseprop= propcontroller.getPropertyController("F_WorkflowField",propname).get(gettype);
   	}else{
   		caseprop=propcontroller.getPropertyController(propname).get(gettype);
   	}
   	 return caseprop;
    	}catch (Error) {
    		console.log("Field Update failed -"+propname);
	        alert ("Source Module: Response Action Adaptor\r\n\r\n"+Error.name+" -"+Error.description+"\r\n"+Error.message+" for -"+propname);
	    return null;
	}
    	
   }
    
    function removeResponseButtons(role,editable){
    	
    	var workItem=editable.icmWorkItem;
		var stepname=editable.icmWorkItem.stepName;
		
		if(role === "Branch Scrutiniser"){
			
			
		}else if(role === "Branch Officer"){
			
			
		}else if(role === "Sales Officer"){
			
		}else if(role === "Branch Manager"){
			
		}else if(role === "Initial Verifier"){
			
		}else if(role === "Credit Assistant"){
			
		}else if(role === "Credit Officer"){
			
		}else if(role === "Credit Approver"){
			
		}else if(role === "Prime Officer"){
			
		}else if(role === "Dispatch officer"){
			
		}else{
			
		}
    		
    		
    }
    
    function removeMandatory(theController) {
    	
    	var propList=theController.getPropertyControllers("F_CaseFolder");
    	for( var i=0;i<propList.length;i++){	
    		propList[i].set("required", false);
    	}
    	return true;
    }
    
});



