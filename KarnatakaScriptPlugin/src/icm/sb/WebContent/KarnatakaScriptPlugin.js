console.log("ScriptPlugin debug start");

require([ "Karnatakascript/Inbasket","Karnatakascript/SearchScripts", "Karnatakascript/AddIPScripts",
		"Karnatakascript/AddTaskScript", "Karnatakascript/CaseDashboard",
		"Karnatakascript/ExportToCSV", "Karnatakascript/Step_Comment",
		"Karnatakascript/Step_ResponseValidation", "Karnatakascript/FieldUpdate",
		"Karnatakascript/BeforeLoadWidgetScriptsRolewise","Karnatakascript/CaseValidationDetails" ], function(ccinbasket,icmscripts,
		addcasescript, addtaskscript, dashboardscript, csvexport,
		cccommentaction, ccresponseaction, propupdateaction, beforeloadaction,cccasevalidation) {
	console.log("ScriptPlugin: end to require files - start");

	console.log("ScriptPlugin: end to require files - end");
});
console.log("ScriptPlugin debug end");